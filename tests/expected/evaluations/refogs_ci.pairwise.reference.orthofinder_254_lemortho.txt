# Command                          : DIST
# File A                           : /raid/user1/seppey/lemortho_validate/benchmark/analysis_outputs/reference.refogs_ci.predictions.tsv
# File B                           : /raid/user1/seppey/lemortho_validate/benchmark/analysis_outputs/orthofinder_254_lemortho.refogs_ci.predictions.tsv
# Reference keys                   : 
# N(threads)                       : 1
# N(pooled)                        : 1
# N(clusters) in A                 : 70
# N(clusters) in B                 : 39
# Nr of threads used  : 1
# Thread pool size    : 1
# Nr of clusters in A : 70
# Nr of clusters in B : 39
# nkeysA = 1354
# nkeysB = 138
# nkeys  = 1354
9.5152e-04
