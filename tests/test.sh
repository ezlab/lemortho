#!/bin/sh
set -eu
export LEM_ORTHO_ROOT=$(pwd)/
mv $LEM_ORTHO_ROOT/benchmark/yaml/orthofinder.refogs_ci.yaml.d $LEM_ORTHO_ROOT/benchmark/yaml/orthofinder.refogs_ci.yaml
rm $LEM_ORTHO_ROOT/benchmark/yaml/orthofinder.refogs_pure.yaml
$LEM_ORTHO_ROOT/workflow/scripts/lem_ortho --cores 1
# here we just test for the presence of the expected files. If any missing, ci fail
cd ${LEM_ORTHO_ROOT}/tests/expected/
for i in $(ls -d analysis_outputs/*);do ls ${LEM_ORTHO_ROOT}/benchmark/$i;done
for i in $(ls -d evaluations/*);do ls ${LEM_ORTHO_ROOT}/benchmark/$i;done
for i in $(ls -d final_results/*);do ls ${LEM_ORTHO_ROOT}/benchmark/$i;done

