import glob
import shutil
import numpy as np
import pandas as pd
import toolbox

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

os.chdir(tmp)

toolname = config["container"].split("/")[-1].split(":")[0]
datasets = config["dataset"]

container_runner, docker_container, clean = toolbox.define_docker_command(
    toolname, root, tmp, config, int(workflow.cores)
)

toolbox.validate_config(config)

# copy the reference as if it was a tool, for the pairwise analysis
if not os.path.exists(
    "{root}analysis_outputs/reference.{dataset}.predictions.tsv".format(
        root=root, dataset=config["dataset"]
    )
):
    shutil.copy(
        "{root}datasets/{dataset}/truth.og".format(
            root=root, dataset=config["dataset"]
        ),
        "{root}analysis_outputs/reference.{dataset}.predictions.tsv".format(
            root=root, dataset=config["dataset"]
        ),
    )
if not os.path.exists(
    "{root}analysis_outputs/reference.{dataset}.runtime_analysis.txt".format(
        root=root, dataset=config["dataset"]
    )
):
    with open(
        "{root}analysis_outputs/reference.{dataset}.runtime_analysis.txt".format(
            root=root, dataset=config["dataset"]
        ),
        "w",
    ) as f:
        f.write("0")
if not os.path.exists(
    "{root}analysis_outputs/reference.{dataset}.memory_analysis.txt".format(
        root=root, dataset=config["dataset"]
    )
):
    with open(
        "{root}analysis_outputs/reference.{dataset}.memory_analysis.txt".format(
            root=root, dataset=config["dataset"]
        ),
        "w",
    ) as f:
        f.write("0")


localrules:
    target,


rule target:
    input:
        predictions=expand(
            "{root}analysis_outputs/{toolname}.{dataset}.predictions.tsv",
            root=root,
            toolname=toolname,
            dataset=datasets,
        ),
        runtime=expand(
            "{root}analysis_outputs/{toolname}.{dataset}.runtime_analysis.txt",
            root=root,
            toolname=toolname,
            dataset=datasets,
        ),
        memory=expand(
            "{root}analysis_outputs/{toolname}.{dataset}.memory_analysis.txt",
            root=root,
            toolname=toolname,
            dataset=datasets,
        ),


rule run_analysis:
    input:
        dataset=expand("{root}datasets/{dataset}", root=root, dataset=datasets),
    output:
        predictions=expand(
            "{root}analysis_outputs/{toolname}.{{dataset}}.predictions.tsv",
            root=root,
            toolname=toolname,
        ),
        runtime=expand(
            "{root}analysis_outputs/{toolname}.{{dataset}}.runtime_analysis.txt",
            root=root,
            toolname=toolname,
        ),
        memory=expand(
            "{root}analysis_outputs/{toolname}.{{dataset}}.memory_analysis.txt",
            root=root,
            toolname=toolname,
        ),
    message:
        "run analysis..."
    container:
        "{}sif/{}.sif".format(root, toolname)
    threads: int(workflow.cores)
    shell:
        """

        # run the tool
        {container_runner} {docker_container} {root}../workflow/scripts/task_wrapper.sh \
        LEM_ORTHO_analysis.sh \
        {wildcards.dataset} {output.predictions} analysis_{wildcards.dataset}

        mv {tmp}memory.analysis_{wildcards.dataset}.txt {output.memory}
        mv {tmp}runtime.analysis_{wildcards.dataset}.txt {output.runtime}

        # remove stopped docker containers
        {clean}

        """
