import os
import sys
import pandas as pd

# these functions are used in the analysis and/or evaluation smk pipelines

def set_root():
    """
    The root is where the benchmark content is produced
    :return: the root path
    """
    root = os.environ["LEM_ORTHO_ROOT"]
    if not root.endswith("/"):
        root += "/"
    root += "benchmark/"
    return root


def set_tmp(root, config):
    """
    The tmp folder is specific to a run, according to the config file for that run
    :param root: the root folder of the LEM_ORTHO install
    :param config: the config file of the run
    :return: the tmp folder to use
    """
    if "tmp" in config:
        tmp = "{}tmp/{}/".format(root, config["tmp"])
    else:
        tmp = "{}tmp/{}/".format(root, config["datetime"])
    try:
        os.mkdir(tmp)
    except FileExistsError:
        pass
    return tmp


def define_docker_command(name, root, tmp, config, cores):
    """
    As LEMOrtho uses either Docker of Singularity but snakemake supports only Singularity,
    this function creates the appropriate command to add in the case of Docker
    :param name: the name of the container (that is without repository and tag)
    :param root: the root path
    :param tmp: the tmp folder path
    :param config: the config dictionary
    :param cores: number of cpus to use
    :return: the docker command to call
    """

    if "--use-singularity" not in sys.argv:
        container_runner = "docker run --memory='{}' --memory-swap='{}' --cpus {} -e cpus={} -u $(id -u) " \
                           "-v {}../:{}../ -w {} --name {}_{}_".format(config['docker_memory'],
                                                                       config['docker_memory'],
                                                                       cores, cores, root, root, tmp, name,
                                                                       config["datetime"])
        docker_container = config['container']
        clean = 'docker rm $(docker ps -a | grep -v "Up " | grep "{}_" | cut -f1 -d" ") || true'.format(
            name
        )
    else:
        container_runner = ""
        clean = ""
        docker_container = ""

    return container_runner, docker_container, clean


def validate_config(config):
    """
    Just a check that forbidden characters are not used, trigger an error if so
    Note: "-" is forbidden, this was a constraint in LEMMI, still here but unecessary
    => I disable the function but I keep it as it may be useful in the future.
    :param config: the config dictionary
    """
    return True
    error_msg = ''
    try:
        if '-' in config['container']:
            error_msg = 'The character - is not allowed in tool names'
    except KeyError:
        pass
    if error_msg != '':
        exit(error_msg)

def to_json(inp, outp, config,  orient="columns", sort_with=False, reverse=False):
    """
    Read the provided tsv and turn it into a json while sorting the values
    :param inp: a path to a tsv file
    :param outp: a path to a json file
    :param config: the config dictionnary
    :param orient: to be passed to pandas to_json()
    :param sort_with: either the keyword 'self',
    or a tsv file of a metric (e.g. F1) to be used to sort everything else in the same order.
    Note, in the code below, we use the value 85 in the column 3 to pick the rows used for the sorting, it is hardcoded.
    So the file provided to do the sort has to have this 3rd column with 0-100 including 85
    :param reverse: reverse the value when sorting
    """
    data = pd.read_csv(inp, sep="\t", header=1)
    if sort_with == 'self':
        data = data.assign(sort_with=1)
        data_sort = data.loc[data['percent_included'] == 85]
        for tool in data_sort['toolname']:
            data.loc[data['toolname']==tool, 'sort_with'] = data_sort.loc[data['toolname']==tool, 'value'].values[0]
    elif sort_with:
        data = data.assign(sort_with=1)
        for line in open(sort_with):
            if line.startswith('#'):
                continue
            if line.split('\t')[1] == config['dataset_name'] and int(line.split('\t')[2]) == 85:
                value = float(line.strip().split('\t')[3])
                tool = line.split('\t')[0]
                data.loc[data['toolname']==tool, 'sort_with'] = value
    pd.set_option('display.max_rows', None)
    try:
        data.sort_values(by=['sort_with', 'toolname'], inplace=True, ignore_index=True, ascending=[True*(not reverse), True*(not reverse)])
        data.drop(["sort_with"], inplace=True, axis=1)
    except KeyError:
        pass
    data.to_json(
        outp, index=True, indent=4, orient=orient
    )
