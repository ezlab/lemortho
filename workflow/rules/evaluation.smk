#
# Note: in this script, the header of some files contains "percent_included" and "value"
# These are misleading labels, the meaning is
# percent_included = the value of the metric considered
# value = the counts of cluster that reach that value
# In the frontend/website, the plots that are based on this percent_included+value are labeled properly
# x=the value considered, y= the counts of clusters
#

import glob
import shutil

import numpy as np
import pandas as pd
import toolbox
import yaml

root = toolbox.set_root()
tmp = toolbox.set_tmp(root, config)

config["container"] = config["lem_ortho_master"]
container_runner, docker_container, clean = toolbox.define_docker_command(
    config["lem_ortho_master"].split("/")[-1].split(":")[0], root, tmp, config, 1
)

os.chdir(tmp)

toolnames = set(
    [
        predictions_path.split("/")[-1].split(".")[0]
        for predictions_path in glob.glob(
            "{}analysis_outputs/*.{}*.predictions.tsv".format(
                root, config["dataset_name"]
            )
        )
        if predictions_path.split("/")[-1].split(".")[0] not in config["no_evaluation"]
    ]
)

toolbox.validate_config(config)


localrules:
    target,


rule target:
    input:
        expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation_classify.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        expand(
            "{root}final_results/{dataset}.memory_analysis.tsv",
            dataset=config["dataset"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset}.memory_analysis.json",
            dataset=config["dataset"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset}.runtime_analysis.tsv",
            dataset=config["dataset"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset}.runtime_analysis.json",
            dataset=config["dataset"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.f1.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.precision.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.precision.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.recall.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.recall.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.json",
            root=root,
            dataset_name=config["dataset_name"],
        ),
        expand(
            "{root}final_results/{dataset_name}.json",
            root=root,
            dataset_name=config["dataset_name"],
        ),
        expand(
            "{root}evaluations/{dataset_name}.pairwise.{toolnamesA}.{toolnamesB}.txt",
            root=root,
            dataset_name=config["dataset_name"],
            toolnamesA=toolnames,
            toolnamesB=toolnames,
        ),
        expand(
            "{root}final_results/{dataset}.pairwise.json",
            dataset=config["dataset"],
            root=root,
        ),


rule do_pairwise:
    message:
        "Producing pairwise distance between clusters..."
    input:
        predictions=expand(
            "{root}analysis_outputs/{toolnames}.{dataset}.predictions.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
    output:
        expand(
            "{root}evaluations/{dataset_name}.pairwise.{{toolnamesA}}.{{toolnamesB}}.txt",
            root=root,
            dataset_name=config["dataset_name"],
        ),
    params:
        dataset=config["dataset_name"],
        root=root,
        docker_suffix=(
            lambda wildcards: "pairwise" + wildcards.toolnamesA + wildcards.toolnamesB
            if container_runner != ""
            else ""
        ),
    container:
        "{}sif/lem_ortho_master.sif".format(root)
    shell:
        """
        # call the LEM_ORTHO master container for the task.
        {container_runner}{params.docker_suffix} {docker_container} /brhclus/bin/cmpclus dist --A {params.root}analysis_outputs/{wildcards.toolnamesA}.{params.dataset}.predictions.tsv  --B {params.root}analysis_outputs/{wildcards.toolnamesB}.{params.dataset}.predictions.tsv > {output} || true
        """


rule compute_pairwise:
    message:
        "fusioning pairwise"
    input:
        tools=expand(
            "{root}evaluations/{dataset_name}.pairwise.{toolnamesA}.{toolnamesB}.txt",
            root=root,
            dataset_name=config["dataset_name"],
            toolnamesA=toolnames,
            toolnamesB=toolnames,
        ),
    output:
        expand(
            "{root}final_results/{dataset}.pairwise.json",
            dataset=config["dataset"],
            root=root,
        ),
    run:
        fields = {"A": [], "B": [], "Distance": []}
        for file in input:
            A = file.split(".")[-3]
            B = file.split(".")[-2]
            for line in open(file):
                if not line.startswith("#"):
                    value = float(line.strip())
            fields["A"].append(A)
            fields["B"].append(B)
            fields["Distance"].append(value)
        data = pd.DataFrame.from_dict(fields)
        try:
            data.sort_values(by=["A", "B"], inplace=True, ignore_index=True)
        except KeyError:
            pass
        data.to_json(output[0], index=True, indent=4, orient="index")


rule do_evaluation:
    message:
        "Evaluating results..."
    input:
        truth=expand(
            "{root}datasets/{dataset}/truth.og",
            root=root,
            dataset=config["dataset_name"],
        ),
        predictions=expand(
            "{root}analysis_outputs/{toolnames}.{dataset}.predictions.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
    output:
        evaluation="{root}evaluations/{toolnames}.{dataset}.evaluation.tsv",
    params:
        dataset=config["dataset_name"],
        root=root,
        docker_suffix=(
            lambda wildcards: "mismatch" + wildcards.toolnames
            if container_runner != ""
            else ""
        ),
    container:
        "{}sif/lem_ortho_master.sif".format(root)
    shell:
        """
        # call the LEM_ORTHO master container for the task.
        {container_runner}{params.docker_suffix} {docker_container} /brhclus/bin/cmpclus mismatch --A {params.root}datasets/{params.dataset}/truth.og --B {params.root}analysis_outputs/{wildcards.toolnames}.{wildcards.dataset}.predictions.tsv > {output.evaluation} || true
        """


rule do_evaluation_classify:
    message:
        "Evaluating results..."
    input:
        truth=expand(
            "{root}datasets/{dataset}/truth.og",
            root=root,
            dataset=config["dataset_name"],
        ),
        predictions=expand(
            "{root}analysis_outputs/{toolnames}.{dataset}.predictions.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
    output:
        evaluation="{root}evaluations/{toolnames}.{dataset}.evaluation_classify.tsv",
    params:
        dataset=config["dataset_name"],
        root=root,
        docker_suffix=(
            lambda wildcards: "classify" + wildcards.toolnames
            if container_runner != ""
            else ""
        ),
    container:
        "{}sif/lem_ortho_master.sif".format(root)
    shell:
        """
        # call the LEM_ORTHO master container for the task.
        {container_runner}{params.docker_suffix} {docker_container} /brhclus/bin/cmpclus classify --A {params.root}datasets/{params.dataset}/truth.og --B {params.root}analysis_outputs/{wildcards.toolnames}.{wildcards.dataset}.predictions.tsv > {output.evaluation} || true
        """


rule memory_analysis_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolnames}.{dataset}.memory_analysis.txt",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        sort=expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{dataset_name}.memory_analysis.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.memory_analysis.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    message:
        "Outputting the memory during analysis table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write("#LEM_ORTHO:v1.0\ntoolname\tdataset\tvalue\n")
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                dataset_name = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    value = inp.read()
                if toolname != "reference":
                    outp.write("{}\t{}\t{}".format(toolname, dataset_name, value))

        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule runtime_analysis_to_final_table:
    input:
        data=expand(
            "{root}analysis_outputs/{toolnames}.{dataset}.runtime_analysis.txt",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        sort=expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{dataset_name}.runtime_analysis.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.runtime_analysis.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    message:
        "Outputting the runtime during analysis table..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write("#LEM_ORTHO:v1.0\ntoolname\tdataset\tvalue\n")
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                dataset_name = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    value = inp.read()
                if toolname != "reference":
                    outp.write("{}\t{}\t{}".format(toolname, dataset_name, value))
        toolbox.to_json(output[0], output[1], config=config, sort_with=input.sort[0])


rule f1_to_final_table:
    input:
        data=expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
    output:
        expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.f1.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    message:
        "Outputting the f1 score..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write("#LEM_ORTHO:v1.0\ntoolname\tdataset\tpercent_included\tvalue\n")
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                dataset_name = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    for line in inp:
                        if "GLOB: HIST" in line:
                            percent_included = line.split(" ")[-10]
                            value = line.split(" ")[-7]
                            outp.write(
                                "{}\t{}\t{}\t{}\n".format(
                                    toolname, dataset_name, percent_included, value
                                )
                            )

        toolbox.to_json(
            output[0], output[1], config=config, sort_with="self", reverse=True
        )


rule recall_to_final_table:
    input:
        data=expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        sort=expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{dataset_name}.recall.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.recall.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    message:
        "Outputting the recall score..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write("#LEM_ORTHO:v1.0\ntoolname\tdataset\tpercent_included\tvalue\n")
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                dataset_name = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    for line in inp:
                        if "GLOB: HIST" in line:
                            percent_included = line.split(" ")[-10]
                            value = line.split(" ")[-9]
                            outp.write(
                                "{}\t{}\t{}\t{}\n".format(
                                    toolname, dataset_name, percent_included, value
                                )
                            )

        toolbox.to_json(
            output[0], output[1], config=config, sort_with=input.sort[0], reverse=True
        )


rule precision_to_final_table:
    input:
        data=expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        sort=expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{dataset_name}.precision.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        expand(
            "{root}final_results/{dataset_name}.precision.json",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    message:
        "Outputting the precision score..."
    threads: 1
    run:
        with open(output[0], "w") as outp:
            outp.write("#LEM_ORTHO:v1.0\ntoolname\tdataset\tpercent_included\tvalue\n")
            for file in input.data:
                toolname = file.split("/")[-1].split(".")[0]
                dataset_name = file.split("/")[-1].split(".")[1]
                with open(file) as inp:
                    for line in inp:
                        if "GLOB: HIST" in line:
                            percent_included = line.split(" ")[-10]
                            value = line.split(" ")[-8]
                            outp.write(
                                "{}\t{}\t{}\t{}\n".format(
                                    toolname, dataset_name, percent_included, value
                                )
                            )

        toolbox.to_json(
            output[0], output[1], config=config, sort_with=input.sort[0], reverse=True
        )


rule detailed_table_classify:
    input:
        data=expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation_classify.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        data2=expand(
            "{root}evaluations/{toolnames}.{dataset}.evaluation.tsv",
            root=root,
            toolnames=toolnames,
            dataset=config["dataset_name"],
        ),
        f1=expand(
            "{root}final_results/{dataset_name}.f1.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        precision=expand(
            "{root}final_results/{dataset_name}.precision.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
        recall=expand(
            "{root}final_results/{dataset_name}.recall.tsv",
            dataset_name=config["dataset_name"],
            root=root,
        ),
    output:
        expand(
            "{root}final_results/{dataset_name}.json",
            root=root,
            dataset_name=config["dataset_name"],
        ),
    message:
        "Producing the detailed table with the data from the classify evaluation"
    threads: 1
    run:
        f1_df = pd.read_csv(input.f1[0], sep="\t", comment="#")
        recall_df = pd.read_csv(input.recall[0], sep="\t", comment="#")
        precision_df = pd.read_csv(input.precision[0], sep="\t", comment="#")
        fields = {
            "toolname": [],
            "Nexact": [],
            "Nakin": [],
            "Nmissed": [],
            "Npoor": [],
            "Nfused": [],
            "Nsplit": [],
            "Ncomplex": [],
            "F185": [],
            "Precision85": [],
            "Recall85": [],
            "SumExactAkin": [],
            "SumFusedSplit": [],
            "SumComplexMissed": [],
            "Fused_events": [],
            "Split_events": [],
            "A": [],
            "B": [],
        }
        for inp in input.data:
            toolname = inp.split("/")[-1].split(".")[0]
            Nexact = -1
            Nakin = -1
            Nmissed = -1
            Npoor = -1
            Nfused = -1
            Nsplit = -1
            Ncomplex = -1
            Fused_events = -1
            Split_events = -1
            for line in open(inp):
                if line.startswith("# N(clusters A)"):
                    A = line.strip().split(" ")[-1]
                elif line.startswith("# N(clusters)"):
                    N = line.strip().split(" ")[-1]
                elif line.startswith("# N(EXACT)"):
                    Nexact = line.strip().split(" ")[-1]
                elif line.startswith("# N(SIMILAR)"):
                    Nakin = line.strip().split(" ")[-1]
                elif line.startswith("# N(MISSED)"):
                    Nmissed = line.strip().split(" ")[-1]
                elif line.startswith("# N(POOR)"):
                    Npoor = line.strip().split(" ")[-1]
                elif line.startswith("# N(MERGED) events"):
                    Fused_events = line.strip().split(" ")[-1]
                elif line.startswith("# N(MERGED) clusters"):
                    Nfused = line.strip().split(" ")[-1]
                elif line.startswith("# N(SPLIT) events"):
                    Split_events = line.strip().split(" ")[-1]
                elif line.startswith("# N(SPLIT) clusters"):
                    Nsplit = line.strip().split(" ")[-1]
                elif line.startswith("# N(COMPLEX)"):
                    Ncomplex = line.strip().split(" ")[-1]
            fields["toolname"].append(toolname)
            fields["Nexact"].append(int(Nexact))
            fields["Nakin"].append(int(Nakin))
            fields["Nmissed"].append(int(Nmissed))
            fields["Npoor"].append(int(Npoor))
            fields["Nfused"].append(int(Nfused))
            fields["Nsplit"].append(int(Nsplit))
            fields["Ncomplex"].append(int(Ncomplex))
            fields["F185"].append(
                int(
                    f1_df[
                        (f1_df["toolname"] == toolname)
                        & (f1_df["percent_included"] == 85)
                    ]["value"].values[0]
                )
            )
            fields["Precision85"].append(
                int(
                    precision_df[
                        (precision_df["toolname"] == toolname)
                        & (precision_df["percent_included"] == 85)
                    ]["value"].values[0]
                )
            )
            fields["Recall85"].append(
                int(
                    recall_df[
                        (recall_df["toolname"] == toolname)
                        & (recall_df["percent_included"] == 85)
                    ]["value"].values[0]
                )
            )
            fields["SumExactAkin"].append(int(Nexact) + int(Nakin))
            fields["SumFusedSplit"].append(int(Nfused) + int(Nsplit))
            fields["SumComplexMissed"].append(int(Ncomplex) + int(Nmissed))
            fields["Fused_events"].append(int(Fused_events))
            fields["Split_events"].append(int(Split_events))
            fields["A"].append(int(A))


        for inp in input.data2:
            for line in open(inp):
                if line.startswith("GLOB: N(clus B involved in a match)"):
                    B = line.strip().split(" ")[-1]
            fields["B"].append(int(B))


        data = pd.DataFrame.from_dict(fields)

        try:
            data.sort_values(by=["toolname"], inplace=True, ignore_index=True)
        except KeyError:
            pass
        data.to_json(output[0], index=True, indent=4, orient="index")
