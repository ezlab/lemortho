Consider [submitting](https://lemortho.ezlab.org/submission) your tool/pipeline for benchmarking. If you develop a tool that is present on the platform, make sure that the last version is sent to us for evaluation.

Report bugs and suggestions as an [issue](https://gitlab.com/ezlab/lemortho/-/issues).
