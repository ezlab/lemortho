# Lemortho

See [https://lemortho.ezlab.org/](https://lemortho.ezlab.org/) and [https://www.ezlab.org/lemortho-documentation.html](https://www.ezlab.org/lemortho-documentation.html)

In short:

```bash
git clone https://gitlab.com/ezlab/lemortho.git
export LEM_ORTHO_ROOT=/your/path/lemortho
export PATH=${LEM_ORTHO_ROOT}/workflow/scripts:$PATH

conda install -n base -c conda-forge mamba
mamba env update -n lemortho --file ${LEM_ORTHO_ROOT}/workflow/envs/lem_ortho.yaml
conda activate lemortho

lem_ortho --cores 8 # running locally on Docker
lem_ortho --cores 8 --use-singularity # running locally on Singularity
```
